<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S2 PHP Activity</title>
</head>
<body>
	<h1>Divisible of 5</h1>
	<?php divFive(); ?>

	<h1>Array Manipulation</h1>
	<?php array_push($students, 'John Smith'); ?>
	<pre><?php print_r($students); ?></pre>

	<p><?= count($students); ?></p>	

	<?php array_push($students, 'Jane Smith'); ?>
	<pre><?php print_r($students); ?></pre>

	<p><?= count($students); ?></p>	

	<?php array_shift($students); ?>
	<pre><?php print_r($students); ?></pre>

	<p><?= count($students); ?></p>	

</body>
</html>